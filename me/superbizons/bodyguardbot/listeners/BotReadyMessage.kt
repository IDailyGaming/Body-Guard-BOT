package me.superbizons.bodyguardbot.listeners

import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class BotReadyMessage : ListenerAdapter() {


    override fun onReady(e: ReadyEvent){
        for (t: TextChannel in e.jda.textChannels) {
            t.sendMessage("Done!").queue()
        }
    }
}