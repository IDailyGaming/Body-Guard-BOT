package me.superbizons.bodyguardbot.listeners

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.events.guild.GuildBanEvent
import net.dv8tion.jda.core.events.guild.GuildUnbanEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberNickChangeEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import java.awt.Color

class InformationsMessage : ListenerAdapter() {


    var textchannelID = 398656024388829184

    fun onUserJoin(e: GuildMemberJoinEvent){
        val member = e.member.nickname.toString()
        val message = MessageBuilder()
                .setEmbed(EmbedBuilder()
                        .setTitle("New user on our Server!")
                        .setDescription("User $member appeared on our Server!")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build())
                .build()
        e.guild.getTextChannelById(textchannelID).sendMessage(message).queue()
    }


    fun onUserLeave(e: GuildMemberLeaveEvent){
        val member = e.member.nickname.toString()
        val message = MessageBuilder()
                .setEmbed(EmbedBuilder()
                        .setTitle("Whose left Server!")
                        .setDescription("Unfortunately user $member left of our Server")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build())
                .build()
        e.guild.getTextChannelById(textchannelID).sendMessage(message).queue()
    }

    fun onUserChangeNick(e: GuildMemberNickChangeEvent){
        val prevNick = e.prevNick.toString()
        val newNick = e.newNick.toString()
        var message = MessageBuilder()
                .setEmbed(EmbedBuilder()
                        .setTitle("User change his nickname!")
                        .setDescription("User $prevNick changed his nickname for $newNick!")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build())
                .build()
        e.guild.getTextChannelById(textchannelID).sendMessage(message).queue()
    }

    fun onUserBanned(e: GuildBanEvent){
        val member = e.user.name.toString()
        val message = MessageBuilder()
                .setEmbed(EmbedBuilder()
                        .setTitle("User Banned!")
                        .setDescription("User $member Banned and kick for our Server!")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build())
                .build()
        e.guild.getTextChannelById(textchannelID).sendMessage(message).queue()
    }

    fun onUserUnBanned(e: GuildUnbanEvent){
        val member = e.user.name.toString()
        val message = MessageBuilder()
                .setEmbed(EmbedBuilder()
                        .setTitle("User UnBanned!")
                        .setDescription("User $member UnBanned!")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build())
                .build()
        e.guild.getTextChannelById(textchannelID).sendMessage(message).queue()
    }

}