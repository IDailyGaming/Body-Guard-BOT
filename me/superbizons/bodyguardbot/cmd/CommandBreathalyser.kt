package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.util.*

class CommandBreathalyser : ListenerAdapter() {

    val random = Random()

    fun rand(from: Int, to: Int) : Int {
        return random.nextInt(to - from) + from
    }

    @SubscribeEvent
    fun commandBreathLyser(e: MessageReceivedEvent){
        val randomNumber = rand(1,10)
        if (e.message.content.toLowerCase().equals("guard!breathalyser")) {
            e.textChannel.sendMessage("$randomNumber promili").queue()
        }
    }
}