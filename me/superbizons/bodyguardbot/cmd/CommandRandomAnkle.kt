package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.io.File
import java.util.*

class CommandRandomAnkle : ListenerAdapter() {


    val random = Random()

    fun rand(from: Int, to: Int) : Int {
        return random.nextInt(to - from) + from
    }

    @SubscribeEvent
    fun onRandomAnkle(e : MessageReceivedEvent){
        val randomNumber = rand(1,6)
        val file = File("C:\\Users\\Antoni\\Desktop\\Body_Guard_BOT\\Kostka\\kostka$randomNumber.png")
        val msg = MessageBuilder()
                .append("Twoja liczba")
                .build()
        if(e.message.content.toLowerCase().equals("guard!kostka")) {
            e.textChannel.sendFile(file,msg)
        }
    }
}