package me.superbizons.bodyguardbot.cmd


/*
    In this command was helping me @Reedzev, thanks! =)
 */


import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color
import java.util.*

class CommandRandomColor : ListenerAdapter() {

    private val random = Random()


    private fun rand(from: Int,to: Int) : Int{
        return random.nextInt(to - from) + from
    }


    @SubscribeEvent
    fun onCommandRandomColor(e: MessageReceivedEvent){
        if (e.message.content.toLowerCase().equals("guard!color")) {
            val color = Color(rand(0,255), rand(0,255), rand(0,255))
            e.message.textChannel.sendMessage(EmbedBuilder()
                    .setTitle("Random Color")
                    .setDescription("**RGB**: ${color.red}, ${color.green}, ${color.blue}" +
                            "\n**HEX**: ${String.format("#%02x%02x%02x", color.red, color.green, color.blue)}")
                    .setColor(color)
                    .setThumbnail("https://dummyimage.com/128x128/${String.format("%02x%02x%02x", color.red, color.green, color.blue)}/000000&text=+")
                    .build()
            ).queue()
        }
    }

}