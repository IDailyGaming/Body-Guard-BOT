package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandBotStats  : ListenerAdapter(){


    @SubscribeEvent
    fun commandBotStats(e: MessageReceivedEvent){
        if (e.message.content.toLowerCase().equals("guard!botstats")) {
            val message = MessageBuilder()
                    .setEmbed(EmbedBuilder()
                            .setTitle("Body Guard BOT - General Stats")
                            .addField("Servers","${e.jda.guilds.size}",true)
                            .addField("Users","${e.jda.users.size}",true)
                            .addField("Roles","${e.jda.roles.size}",true)
                            .addField("TextChannels","${e.jda.textChannels.size}",true)
                            .addField("VoiceChannels","${e.jda.voiceChannels.size}",true)
                            .setColor(Color.WHITE)
                            .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                            .build())
                    .build()
            e.textChannel.sendMessage(message).queue()
        }
    }
}