package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color
import java.io.File
import java.net.URL
import javax.imageio.ImageIO
import java.awt.image.BufferedImage



class CommandMeow : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandMeow(e: MessageReceivedEvent,vararg args: String){
        if (e.message.content.toLowerCase().equals("guard!meow")) {
            val url = URL("http://random.cat/i/AS1or.jpg");
            val img = ImageIO.read(url)
            val file = File("downloaded.jpg")
            ImageIO.write(img, "jpg", file)

            val msg = MessageBuilder()
                        .setEmbed(EmbedBuilder()
                                .setTitle("Body Guard BOT - Help")
                                .setDescription("Komendy Body Guard Bot'a")
                                .setColor(Color(15103806))
                                .setImage("http://random.cat/i/AS1or.jpg")
                                .build())
                        .build()
            e.textChannel.sendMessage(msg).queue()
        }
    }
}
