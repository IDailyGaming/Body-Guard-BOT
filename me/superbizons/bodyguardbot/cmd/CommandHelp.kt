package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color


class CommandHelp : ListenerAdapter() {

    @SubscribeEvent
    fun CommandHelp(e: MessageReceivedEvent) {
        val cmdHelpMsg = " help - lists all available commands"
        val cmdAvatarMsg = "avatar - shows your or mentioned member avatar"
        val cmdWhoIs = "whois - shwos mentioned User statistic's"
        val cmdAlkomatMsg = "breathalyser - shows how drunk you are (in promilles)"
        val cmdCheckMsg = "check - shows stats of the Discord server"
        val cmdSay = "say - bot saying your words"
        val cmdColor = "color - shows you random color"
        val cmdKick = "kick - kicked member"
        val cmdBan = "ban - banned member"
        val cmdBanner = "banner - zooming your letters and numbers"
        val cmdBanList = "banlist - shows you users wchich are banned on your discord server"
        val authorInfo = "Bot author - Superbizons"
        val cmdBotStatsMsg = "botstats - shows stats of the Body Guard BOT"

        if (e.message.content.toLowerCase().equals("guard!help")) {
            val message = MessageBuilder()
                    .setEmbed(EmbedBuilder()
                            .setTitle("Body Guard BOT - Help")
                            .setDescription("Komendy Body Guard Bot'a")
                            .setColor(Color.WHITE)
                            .addField("Music:"," In Future",false)
                            .addField("Commands:","$cmdHelpMsg \n$cmdWhoIs \n$cmdCheckMsg \n$cmdBotStatsMsg \n$cmdSay ",false)
                            .addField("Fun Commands:","$cmdAlkomatMsg \n$cmdBanner \n$cmdAvatarMsg \n$cmdColor",false)
                            .addField("Admin Commands:","$cmdKick \n$cmdBan \n$cmdBanList",false)
                            .addField("Author:","$authorInfo",false)
                            .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                            .build())
                    .build()
            e.textChannel.sendMessage(message).queue()
        }
    }
}