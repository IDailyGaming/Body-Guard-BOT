package me.superbizons.bodyguardbot.cmd

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandSay : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandSay(e: MessageReceivedEvent) {
        var args = e.message.content.split(' ')

        if (args[0].toLowerCase().equals("guard!say")) {

            if (args.size == 1) {
                val msg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setDescription("guard!say [<text>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()

                e.textChannel.sendMessage(msg).queue()
                return
            }

            e.textChannel.sendMessage(BodyGuardUtils.joinArgs(" ",args,1)).queue()
        }
    }
}