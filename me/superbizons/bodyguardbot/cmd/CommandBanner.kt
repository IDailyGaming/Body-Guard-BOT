package me.superbizons.bodyguardbot.cmd

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandBanner : ListenerAdapter() {


    @SubscribeEvent
    fun commandBanner(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')
        var chars =  CharArray(1500)

        if (args[0].toLowerCase().equals("guard!banner")) {
            var builder = StringBuilder()

            if (args.size == 1) {
                val msg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setDescription("guard!banner [<text>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(msg).queue()
                return
            }
            chars = BodyGuardUtils.joinArgs(" ",args,1).toCharArray()

            for (i in 0 until chars.size) {
                val char = chars.get(i).toLowerCase()
                if (chars.get(i) == '0')  builder.append(":zero:")
                else if (chars.get(i) == '1')  builder.append(":one:")
                else if (chars.get(i) == '2')  builder.append(":two:")
                else if (chars.get(i) == '3')  builder.append(":three:")
                else if (chars.get(i) == '4')  builder.append(":four:")
                else if (chars.get(i) == '5')  builder.append(":five:")
                else if (chars.get(i) == '6')  builder.append(":six:")
                else if (chars.get(i) == '7')  builder.append(":seven:")
                else if (chars.get(i) == '8')  builder.append(":eight:")
                else if (chars.get(i) == '9')  builder.append(":nine:")
                else if (char == 'ą')  builder.append(":regional_indicator_a:")
                else if (char == 'ć')  builder.append(":regional_indicator_c:")
                else if (char == 'ę')  builder.append(":regional_indicator_e:")
                else if (char == 'ł')  builder.append(":regional_indicator_l:")
                else if (char == 'ó')  builder.append(":regional_indicator_o:")
                else if (char == 'ż' || char == 'ź')  builder.append(":regional_indicator_z:")
                else if (char == 'ś')  builder.append(":regional_indicator_s:")
                else if (chars.get(i) == ' ')  builder.append("  ")
                else builder.append(":regional_indicator_${chars.get(i).toLowerCase()}:")
            }
            e.textChannel.sendMessage(builder.toString()).queue()
        }
    }
}