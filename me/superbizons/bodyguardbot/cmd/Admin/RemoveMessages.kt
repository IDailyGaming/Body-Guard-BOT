package me.superbizons.bodyguardbot.cmd.Admin

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color


class RemoveMessages : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandRemoveMessages(e: MessageReceivedEvent){
       val args = e.message.content.split(' ')

        if (args[0].equals("guard!rm")) {

            if (args.size == 1) {
                val msg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setTitle("Body Guard BOT - Command Remove Messages ")
                        .setDescription("guard!rm [<value>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(msg).queue()
                return
            }

            BodyGuardUtils.removeMessages(args[2].toInt(),e.textChannel.name,e.guild.controller)
        }
    }
}