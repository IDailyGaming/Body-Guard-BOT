package me.superbizons.bodyguardbot.cmd.Admin

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandKick : ListenerAdapter() {


    @SubscribeEvent
    fun onCommandKick(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')

        if (args[0].toLowerCase().equals("guard!kick")) {

            if (!(e.member.hasPermission(Permission.KICK_MEMBERS))) {
                e.textChannel.sendMessage("You don't have enough permissions!").queue()
                return
            }
            if (!(e.guild.selfMember.hasPermission(Permission.KICK_MEMBERS))){
                e.textChannel.sendMessage("Bot hasn't got enough permissions!").queue()
                return
            }
            if (args.size == 1 || args.size == 2) {
                val infoMsg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setDescription("guard!kick [<member>] [<reason>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(infoMsg).queue()
                return
            }

            val kickUser = e.message.mentionedUsers[0]

            e.guild.controller.kick(kickUser.id).reason(BodyGuardUtils.joinArgs(" ",args,2)).queue()

            e.textChannel.sendMessage("${e.author.name} kicked ${kickUser.name} from reason: ${BodyGuardUtils.joinArgs(" ",args,2)}").queue()
        }
    }
}