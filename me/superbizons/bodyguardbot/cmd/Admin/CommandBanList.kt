package me.superbizons.bodyguardbot.cmd.Admin

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import net.dv8tion.jda.core.managers.GuildController
import java.awt.Color

class CommandBanList : ListenerAdapter() {

    private fun banList(delimiter: String, args: List<User>, valueOfArgs: Int): String {
        val builder = StringBuilder()
        for (i in valueOfArgs until args.size) {
            builder.append("$i. ${args[i].name}")
            builder.append(delimiter)
        }

        val result = builder.toString()
        return result.substring(0, result.length - delimiter.length)
    }

    @SubscribeEvent
    fun onCommandBanList(e: MessageReceivedEvent){
        if (e.message.content.toLowerCase() == "guard!banlist") {
            if (!(e.member.hasPermission(Permission.BAN_MEMBERS))) {
                e.textChannel.sendMessage("You don't have enough permissions!").queue()
                return
            }
            if (!(e.guild.selfMember.hasPermission(Permission.BAN_MEMBERS))) {
                e.textChannel.sendMessage("Body Guard BOT hasn't got enough permissions!").queue()
                return
            }
            val banListMsg = MessageBuilder().setEmbed(EmbedBuilder()
                    .setTitle("Body Guard BOT - ${e.guild.name}'s Ban List")
                    .setDescription(banList("\n",e.guild.bans.complete(),0))
                    .setColor(Color.WHITE)
                    .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                    .build()).build()
            e.textChannel.sendMessage(banListMsg).queue()
        }
    }
}