package me.superbizons.bodyguardbot.cmd.Admin

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandUnBan : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandUnBan(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')

        if (args[0].toLowerCase().equals("guard!unban")) {
            if (!(e.member.hasPermission(Permission.BAN_MEMBERS))) {
                e.textChannel.sendMessage("You have enough permissions!").queue()
                return
            }
            if (args.size != 2) {
                val infoMsg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setTitle("Body Guard BOT - Command UnBan")
                        .setDescription("guard!unban [<member>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(infoMsg).queue()
                return
            }

            val unBanMember = e.message.mentionedUsers[0]

            e.guild.controller.unban(unBanMember.id).queue()

            e.textChannel.sendMessage("${e.author.name} unbanned ${unBanMember.name}").queue()
        }
    }
}