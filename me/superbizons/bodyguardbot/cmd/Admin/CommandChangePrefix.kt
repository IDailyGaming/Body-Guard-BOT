package me.superbizons.bodyguardbot.cmd.Admin

import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent

class CommandChangePrefix : ListenerAdapter(){
    var settingsPrefix = HashMap<String,Int>()



    @SubscribeEvent
    fun cmdChangePrefix(e: MessageReceivedEvent){
        var content = e.message.content

        if (content.startsWith("guard!prefix")) {
            if (!e.member.isOwner) {
                e.textChannel.sendMessage("You don't have permissions!").queue()
            }
            var tetMessage = content.split(" ")

            e.textChannel.sendMessage(tetMessage.toString()).queue()

        }
    }

}