package me.superbizons.bodyguardbot.cmd.Admin

import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent

class CommandBan : ListenerAdapter() {


    @SubscribeEvent
    fun onCommandBan(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')

        if (args[0].toLowerCase().equals("guard!ban")) {
            if (!(e.member.hasPermission(Permission.BAN_MEMBERS))) {
                e.textChannel.sendMessage("You don't have enough permissions!").queue()
                return
            }
            if (!(e.guild.getMember(e.jda.selfUser).hasPermission(Permission.BAN_MEMBERS))){
                e.textChannel.sendMessage("Bot hasn't got enough permissions!").queue()
                return
            }
            if (args.size != 4) {
                val infoMsg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setTitle("Body Guard BOT - Command Ban")
                        .setDescription("guard!ban [<member>] [<time>] [<reason>]")
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(infoMsg).queue()
                return
            }

            val banMember = e.message.mentionedUsers[0]

            e.guild.controller.ban(banMember.id,args[2].toInt()).reason(BodyGuardUtils.joinArgs(" ",args,3)).queue()

            e.textChannel.sendMessage("${e.author.name} banned ${banMember.name} for ${args[2].toInt()} seconds. Reason: ${BodyGuardUtils.joinArgs(" ",args,3)}").queue()

        }
    }
}