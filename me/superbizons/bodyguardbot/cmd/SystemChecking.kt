package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color


class SystemChecking : ListenerAdapter() {


    @SubscribeEvent
    fun onSystemChecking(e: MessageReceivedEvent){
        if (e.message.content.toLowerCase().equals("guard!check")) {

            val msg = MessageBuilder()
                    .setEmbed(EmbedBuilder()
                            .setTitle("Body Guard BOT - Server Info")
                            .setDescription("")
                            .addField("Server Name","${e.guild.name}",false)
                            .addField("Owner","${e.guild.owner}",false)
                            .addField("Roles","${e.guild.roles.size}",false)
                            .addField("Users","${e.guild.members.size}",false)
                            .setColor(Color.WHITE)
                            .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                            .build())
                    .build()
            e.textChannel.sendMessage(msg).queue()
        }
    }
}