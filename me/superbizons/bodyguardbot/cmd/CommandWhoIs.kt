package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.OnlineStatus
import net.dv8tion.jda.core.entities.Game
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandWhoIs : ListenerAdapter() {

    private var nameOfGame = ""

    @SubscribeEvent
    fun onCommandWhoIs(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')
        val author = e.member

        if (args[0].toLowerCase().equals("guard!whois")) {
            if (args.size == 1) {
                val Msg = MessageBuilder().setEmbed(EmbedBuilder()
                        .setTitle("Body Guard BOT - Command Who Is")
                        .setDescription("guard!whois [<member>]")
                        .setColor(Color.WHITE)
                        .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                        .build()).build()
                e.textChannel.sendMessage(Msg).queue()
                return
            }
            var markMember = e.message.mentionedUsers[0]

            asMemberPlayingGame(e.guild.getMember(markMember))
            val informateMessage = MessageBuilder()
                    .setEmbed(EmbedBuilder()
                            .setTitle("Body Guard BOT - Member Informations")
                            .setColor(Color.WHITE)
                            .addField("Nickname: ","${markMember.name}",false)
                            .addField("OnlineStatus: ","${memberOnlineStatus(e.guild.getMember(markMember))}",false)
                            .addField("Guild Join Date: ","${membersJoinDate(e.guild.getMember(markMember))}",false)
                            .addField("Game: ","${nameOfGame}",false)
                            .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                            .build())
                    .build()

            e.textChannel.sendMessage(informateMessage).queue()
        }

    }


    private fun asMemberPlayingGame(e: Member) = e.game?.name ?: "Nothing"

    private fun memberOnlineStatus(e: Member): String = e.onlineStatus.name

    private fun membersJoinDate(e: Member) = e.joinDate

}