package me.superbizons.bodyguardbot.cmd

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent
import java.awt.Color

class CommandAvatar : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandAvatar(e: MessageReceivedEvent){
        val args = e.message.content.split(' ')
        if (args[0].toLowerCase().equals("guard!avatar")) {
            if (args.size == 1) {
                val msg = MessageBuilder()
                        .setEmbed(EmbedBuilder()
                                .setColor(Color.WHITE)
                                .setTitle("Body Guard BOT - ${e.author.name}'s Avatar",e.author.avatarUrl)
                                .setImage(e.author.avatarUrl)
                                .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                                .build())
                        .build()
                e.textChannel.sendMessage(msg).queue()
                return
            }
            val mentionedUser = e.message.mentionedUsers[0]

            if (mentionedUser.name == ("everyone") || mentionedUser.name == ("here")) {
                e.textChannel.sendMessage("You can't checked everyone's avatars!").queue()
                return
            }
            val msg = MessageBuilder()
                    .setEmbed(EmbedBuilder()
                            .setColor(Color.WHITE)
                            .setTitle("Body Guard BOT - ${mentionedUser.name}'s Avatar",mentionedUser.avatarUrl)
                            .setImage(mentionedUser.avatarUrl)
                            .setFooter(e.jda.selfUser.name,e.jda.selfUser.avatarUrl)
                            .build())
                    .build()
            e.textChannel.sendMessage(msg).queue()
        }
    }


}