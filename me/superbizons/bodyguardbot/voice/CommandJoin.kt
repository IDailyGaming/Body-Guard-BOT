package me.superbizons.bodyguardbot.voice

import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.hooks.SubscribeEvent

class CommandJoin : ListenerAdapter() {

    @SubscribeEvent
    fun onCommandJoin(e: MessageReceivedEvent){
        if (e.message.content.toLowerCase() == "guard!join") {
            AudioUtils().connectToChannel(e.member.voiceState.channel,e.guild.audioManager,e.textChannel,e.author)
            return
        }
        if (e.message.content.toLowerCase() == "guard!disconnect") {
            AudioUtils().disconnectFromChannel(e.guild.audioManager)
        }
    }
}