package me.superbizons.bodyguardbot.voice

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.entities.VoiceChannel
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.managers.AudioManager

class AudioUtils : ListenerAdapter() {

    var guilds = HashMap<Guild,User>()

    fun connectToChannel(channel: VoiceChannel,audioManager: AudioManager,textChannel: TextChannel,user: User){
        if (channel == null){
            textChannel.sendMessage("You must be on Voice Channel!").queue()
            return
        }
        audioManager.openAudioConnection(channel)
    }

    fun disconnectFromChannel(audioManager: AudioManager){
        audioManager.closeAudioConnection()
    }
}