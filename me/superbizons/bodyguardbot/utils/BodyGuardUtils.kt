package me.superbizons.bodyguardbot.utils

import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.managers.GuildController
import java.util.*
import kotlin.collections.ArrayList

object BodyGuardUtils {

    private val scanner = Scanner(System.`in`)

    private val botPrefix = "guard!"
    private val botName = "Body Guard"
    private val random = Random()


    fun name() :String = botName

    fun prefix() :String = botPrefix

    fun toEnglishWords(args: CharArray){
       // future
    }

    private fun rand(from: Int,to: Int) : Int{
        return random.nextInt(to - from) + from
    }

    fun registerToken(): String? {
        println("You must set Token!")
        var args = scanner.nextLine().split(' ')

        if (args[0].equals("java") && args[1].equals("-token")) {
            return args[2]
        }
        return null
    }

    fun removeMessages(value: Int,textChannelName: String,gc: GuildController){

        for (t: TextChannel in gc.guild.getTextChannelsByName(textChannelName,true)) {
            var messages = ArrayList<String>()
            for (i in 1 until value) {
                messages.add(t.latestMessageId)
                t.deleteMessagesByIds(messages).queue()
            }
        }
    }

    fun joinArgs(delimiter: String, args: List<String>,valueOfArgs: Int): String {
        val builder = StringBuilder()
        for (i in valueOfArgs until args.size) {
            builder.append(args[i])
            builder.append(delimiter)
        }

        val result = builder.toString()
        return result.substring(0, result.length - delimiter.length)
    }
}
