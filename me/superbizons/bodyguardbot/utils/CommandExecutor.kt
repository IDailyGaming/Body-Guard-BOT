package me.superbizons.bodyguardbot.utils

import net.dv8tion.jda.core.hooks.SubscribeEvent
import sun.plugin2.message.Message

interface CommandExecutor {

    private fun isCommand(message: Message):Boolean {
        if (message.toString().startsWith("guard!")) {
            return true
        }
        return false
    }

    @SubscribeEvent
    fun onCommand(CommandSender: String,Command: String,Label: String,vararg Arguments: String): Boolean{
        return false
    }
}