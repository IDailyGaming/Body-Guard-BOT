package me.superbizons.bodyguardbot

import me.superbizons.bodyguardbot.cmd.*
import me.superbizons.bodyguardbot.cmd.Admin.*
import me.superbizons.bodyguardbot.cmd.CommandRandomColor
import me.superbizons.bodyguardbot.cmd.SystemChecking
import me.superbizons.bodyguardbot.listeners.BotReadyMessage
import me.superbizons.bodyguardbot.listeners.InformationsMessage
import me.superbizons.bodyguardbot.utils.BodyGuardUtils
import me.superbizons.bodyguardbot.voice.CommandJoin
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.entities.Game
import net.dv8tion.jda.core.hooks.AnnotatedEventManager



fun main(vararg args: String){
    var discord: JDA? = null

    discord = JDABuilder(AccountType.BOT)
            .setToken(BodyGuardUtils.registerToken())
            .setAutoReconnect(true)
            .setEventManager(AnnotatedEventManager())
            .setAudioEnabled(true)
            .setBulkDeleteSplittingEnabled(false)
            .buildBlocking()

    discord.getPresence().setGame(Game.of("guard!help", "https://www.twitch.tv/#"))
    //==========================COMMANDS===========================================
    discord.addEventListener(CommandHelp())
    discord.addEventListener(CommandRandomAnkle())
    discord.addEventListener(CommandAvatar())
    discord.addEventListener(CommandMeow())
    discord.addEventListener(CommandBreathalyser())
    discord.addEventListener(SystemChecking())
    discord.addEventListener(CommandBotStats())
    discord.addEventListener(CommandBanner())
    discord.addEventListener(CommandRandomColor())
    discord.addEventListener(CommandWhoIs())
    discord.addEventListener(CommandSay())
    discord.addEventListener(RemoveMessages())
    discord.addEventListener(CommandKick())
    discord.addEventListener(CommandUnBan())
    discord.addEventListener(CommandBanList())
    discord.addEventListener(CommandBan())
    //===============================LISTENERS=====================================
    discord.addEventListener(InformationsMessage())
    discord.addEventListener(BotReadyMessage())
    //================================MUSIC========================================
    discord.addEventListener(CommandJoin())

}